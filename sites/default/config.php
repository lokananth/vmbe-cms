<?php  

global $config; 
global $base_url; 
global $language ;
$lang_name = $language->language ;

include_once 'api_helpers.php';

// Custom module configuration
$config['brand'] = 1; //For Vectone
$config['productCode'] = "WEB";
$config['sitecode'] = "MBE";
$config['topup_sitecode'] = "BE1";
$config['topup_made_by'] = "VMBE-web-topup";
$config['Auto_Topup_MinLevelID'] = 1;
$config['channel_source'] = "VMBE Website";
$config['subscriberchannel'] = "Standard/Micro Sim";
$config['recomend_subscriber_channel'] = "RECOM_SEND_FRIEND_FREESIM";
$config['sourcereg'] = "VMBE Website";
$config['country'] = "Belgium";
$config['countrycode'] = "BEL";
$config['country_code']="BE";
$config['rates_countrycode'] = "VMBE";
$config['product'] = "VMBE";
$config['dialing_code'] = "32";
$config['lang_code'] = "en";
$config['currency_symbol'] = "&euro;";
$config['currency'] = "EUR";
$config['currencyCode'] = "&#163;";

$config['paytype_paym']="PAYM";
$config['paytype_payg']="PAYG";
$config['paym_country_code'] = "GBR";
$config['paym_subscriberchannel'] = "POSTPAID";

$config['customer_service_free_number'] = "1976";
$config['customer_service_alternate_number'] = "0466591976";


//Rates Module API
define("VM_INTERNATIONAL_RATES", "/Getstandardsmartrates");
define("VM_MOBILE_OPERATOR", "http://192.168.2.102:9706/api/v1/OperatorList");
define("VM_CHEAPCALL_RATES", "/Dmcheapcallratesgetsp");
define("VM_UKRATES", "http://192.168.2.102:9706/api/v1/UKRates");
//define("VM_ROAMINGRATES", "http://192.168.2.102:9706/api/v1/RoamingRates");
define("VM_ROAMINGRATES", "/Websitecentralgetroamingratesinfo");
define("VM_SMART_ROAMINGRATES", "http://192.168.2.102:9706/api/v1/SmartRoamingRates");
define("VM_NATIONAL_RATES", "/Websitecentralratenational");

//VM Features - Operators list by Country
define("VM_COUNTRIES_BY_CODE", "/Websitecentralgetcountrynamecode");
define("VM_OPERATORS_BY_CCODE", "/Websitecentralgetoperatorname");

//VM Features - Mobile Internet
define("VM_HANDSET_MANUFACTURERS", "/Websitecentralgetmobilenamecode");
define("VM_MOBILE_MODELS", "/Websitecentralgetmobilemodelcode");
define("VM_MOBILE_MODEL_DISCURSION", "/Websitecentralgetmobilediscursion");
define("VM_MOBILE_DATA_ACTIVATION", "/Dataactivation");

//Myaccount login url
$config['myaccount_login_url'] = "http://stagingmyaccount.vectonemobile.be";

// MyAccount module
define('API_HOST', 'http://10.22.4.56:1018/v1/user/');
define('VM_MYACCOUNT_LOGIN_PWD_ENCRYPT', 'http://192.168.2.102:9038/v1/user/8ak37gJUYq2hRz/Encrypt');
define('VM_MYACCOUNT_FORGOT_PASSWORD_API', '/Websitecentralforgotpasswordbymobile');
define('VM_MYACCOUNT_LOGIN_CHECK_API', '/Signin');
define('VM_REGISTERED_AND_BRAND_VERIFY_API', '/Websitecentralregistercheckmsisdn');
define('VM_MYACCOUNT_SEND_PIN_API', '/Sendpin'); 
define('VM_MYACCOUNT_REGISTER', '/Websitecentralregisterpersonalinfo');

// Freesim order
define('VM_FREESIM_ORDER', '/Websitecentralfreesimorder');
define('VM_FREESIM_ORDER_REFERRER', '/Websitecentralreferralordernewsim');

//Topup
define('VM_CHECK_ACTIVE_NUMBER', '/Websitecentraltopupcheckactivemobileno');
define('VM_TOPUP_DENOMINATIONS', '/Webtopupamountspmyaccountdm');
define('VM_TOPUP_STEP1', '/CTACCTopupStep1');
define('VM_TOPUP_STEP2', '/CTACCTopupStep2');
define('VM_TOPUP_ADDCREDIT', '/Websitecentraltopupprocessbyccdcwithsmsv2');
define('VM_TOPUP_PAYMENT_LOG', '/Websitecentraltopuppaymentloginsert');
define('VM_AUTO_TOPUP', '/Websitecentralenableautotopup');
define('VM_AUTO_TOPUP_LOG', '/Websitecentralautotopuplog');
define('VM_FAILURE_PAYMENT_UPDATE', '/Websitecentraltopupinsertpaymentfailedtransaction');
define('VM_TOPUP_GET_BALANCE', '/Websitecentraltopupgettopuplogdetails');
define('VM_TOPUP_STATUS_SMS', '/SendTopupStatusSMS');

//Auto Topup API
define('DMNL_MYACCOUNT_ADD_NEW_ORDER', 'http://192.168.2.102:9706/api/v1/AddNewOrder');
define('DMNL_MYACCOUNT_PAY_AUTO_TOPUP', 'http://192.168.2.102:9706/api/v1/PayAutoTopup');

define('DMNL_MYACCOUNT_DELETE_AUTO_TOPUP', 'http://192.168.2.102:9706/api/v1/DeleteAutoTopup');
 
//Refer a Friend Bonus Topup API
define('VM_RERERAFRIEND_BONUS_TOPUP', '/Websitecentralrfrreferraltopup');

// PaySafe Implemenation API
define('PAYSAFE_API_KEY' , 'http://192.168.2.102:9810/api/v1/Key');
define('PAYSAFE_CARDURL' , 'http://192.168.2.102:9810/api/v1/PaySafe');
define('PAYSAFE_INFO', 'http://192.168.2.102:9810/api/v1/PaySafeInfo');
define('PAYSAFE_GETSERIAL' , 'http://192.168.2.102:9810/api/v1/PaySafeGetSerial');
define('PAYSAFE_EXECUTEDEBIT' , 'http://192.168.2.102:9810/api/v1/PaySafeExecuteDebit');
define('PAYSAFE_CANCEL' , 'http://192.168.2.102:9810/api/v1/PaySafeCancel');

define('PAYSAFE_CURRENCY_EXCHANGE' ,  'http://192.168.2.102:9810/api/v1/InsertCurrencyExchange');
define('PAYSAFE_CURRENCY_UPDATE' , 'http://192.168.2.102:9810/api/v1/CurrencyUpdate');
define('PAYSAFE_GET_ERROR_CODE', 'http://192.168.2.102:9810/api/v1/ErrorMsgGet');
define('PAYSAFE_LOG_INSERT', 'http://192.168.2.102:9810/api/v1/PaymentLogInsert');
define('PAYSAFE_INSERT_MER_REF_DETAILS', 'http://192.168.2.102:9810/api/v1/InsertMerchantRefDtls');
define('PAYSAFE_INSERT_PNURL_LOG', 'http://192.168.2.102:9810/api/v1/InsertPnurlLog');
define('PAYSAFE_GET_PNRUL_LOG', 'http://192.168.2.102:9810/api/v1/GetPnurlLog');


//Site urls
if($lang_name=="en")
{
	$config['contact_url'] = "/en/contact-us";
	$config['tc_url'] = "/en/terms-and-conditions";
	$config['pp_url'] = "/en/privacy-policy";
	$config['getting_started_url'] = "/en/getting-started";
	$config['bundles_url'] = "/en/national-bundles";
	$config['rates_url'] = "/en/exclusive-low-rates";
}	
else if($lang_name=="nl")
{
	$config['contact_url'] = "/nl/contact";
	$config['tc_url'] = "/nl/terms-and-conditions";
	$config['pp_url'] = "/nl/privacy-policy";
	$config['getting_started_url'] = "/nl/ga-van-start";
	$config['bundles_url'] = "/nl/nationale-bundel";
	$config['rates_url'] = "/nl/internationale-prepaid-tarieven";
}	
else	
{
	$config['contact_url'] = "/contact-us";
	$config['tc_url'] = "/conditions-générales";
	$config['pp_url'] = "/politique-de-confidentialité";
	$config['getting_started_url'] = "/getting-started";
	$config['bundles_url'] = "/forfait-nationaux";
	$config['rates_url'] = "/tarifs-internationaux";
}	
	

$config['ccode'] = array('Afghanistan'=>'AFG',
'Albania'=>'ALB',
'Algeria'=>'DZA',
'AmericanSamoa'=>'ASM',
'Andorra'=>'AND',	
'Angola'=>'AGO',
'Anguilla'=>'AIA',
'Antarctica'=>'ATA',
'AntiguaBarbuda'=>'ATG',
'Argentina'=>'ARG',
'Armenia'=>'ARM',
'Aruba'=>'ABW',
'Ascension'=>'ASC',
'Australia'=>'AUS',
'Austria'=>'AUT',
'Azerbaijan'=>'AZE',
'Bahamas'=>'BHS',
'Bahrain'=>'BHR',
'Bangladesh'=>'BGD',
'Barbados'=>'BRB',
'Belarus'=>'BLR',
'Belgium'=>'BEL',
'Belize'=>'BLZ',
'Benin'=>'BEN',
'Bermuda'=>'BMU',
'Bhutan'=>'BTN',
'Bolivia'=>'BOL',
'Bosniaherzegovina'=>'BIH',
'Bosnia Herzegovina'=>'BIH',
'Botswana'=>'BWA',
'Brazil'=>'BRA',
'BritishVirginIslands'=>'VGB',
'Bulgaria'=>'BGR',
'BurkinaFaso'=>'BFA',
'Burundi'=>'BDI',
'Cambodia'=>'KHM',
'Cameroon'=>'CMR',
'Canada'=>'CAN',
'CanaryIslands'=>'ICT',
'CapeVerde'=>'CPV',
'CaymanIslands'=>'CYM',
'CentralAfricanRepublic'=>'CAF',
'Chad'=>'TCD',
'Chattham Islands'=>'CMI',
'Chile'=>'CHL',
'China'=>'CHN',
'ChristmasIslands'=>'CXR',
'CocosIslands'=>'CCK',
'Colombia'=>'COL',
'Comoros'=>'COM',
'Congo'=>'COG',
'CongoCDR'=>'COD',
'CookIslands'=>'COK',
'CostaRica'=>'CRI',
'Croatia'=>'HRV',
'Cuba'=>'CUB',
'Cyprus'=>'CYP',
'CyprusNorth'=>'CYN',
'CzechRepublic'=>'CZE',
'Czechrepublic'=>'CZE',
'Denmark'=>'DNK',
'DiegoGarcia'=>'DGA',
'Djibouti'=>'DJI',
'Dominica'=>'DMA',
'DominicanRepublic'=>'DOM',
'EastTimor'=>'TMP',
'Ecuador'=>'ECU',
'Egypt'=>'EGY',
'ElSalvador'=>'SLV',
'EquatorialGuinea'=>'GNQ',
'Eritrea'=>'ERI',
'Estonia'=>'EST',
'Ethiopia'=>'ETH',
'FalklandIslands'=>'FLK',
'FaroeIslands'=>'FRO',
'Fiji'=>'FJI',
'Finland'=>'FIN',
'France'=>'FRA',
'FrenchGuiana'=>'GUF',
'FrenchPolynesia'=>'PYF',
'Gabon'=>'GAB',
'Gambia'=>'GMB',
'Georgia'=>'GEO',
'Germany'=>'DEU',
'Ghana'=>'GHA',
'Gibraltar'=>'GIB',
'Greece'=>'GRC',
'Greenland'=>'GRL',
'Grenada'=>'GRD',
'Guadeloupe'=>'GLP',
'Guam'=>'GUM',
'Guantanamo'=>'GTO',
'Guatemala'=>'GTM',
'GuineaBissau'=>'GNB',
'GuineaRepublic'=>'GIN',
'Guyana'=>'GUY',
'Haiti'=>'HTI',
'Honduras'=>'HND',
'Hongkong'=>'HKG',
'Hungary'=>'HUN',
'Iceland'=>'ISL',
'India'=>'IND',
'Indonesia'=>'IDN',
'Iran'=>'IRN',
'Iraq'=>'IRQ',
'Ireland'=>'IRL',
'Israel'=>'ISR',
'Italy'=>'ITA',
'IvoryCoast'=>'CIV',
'Jamaica'=>'JAM',
'Japan'=>'JPN',
'Jordan'=>'JOR',
'Kazakhstan'=>'KAZ',
'Kenya'=>'KEN',
'Kiribati'=>'KIR',
'NorthKorea'=>'PRK',
'KoreaSouth'=>'KOR',
'SouthKorea'=>'KOR',
'Kosovo'=>'UNK',
'Kuwait'=>'KWT',
'Kyrgyzstan'=>'KGZ',
'Laos'=>'LAO',
'Latvia'=>'LVA',
'Lebanon'=>'LBN',
'Lesotho'=>'LSO',
'Liberia'=>'LBR',
'Libya'=>'LBY',
'Liechtenstein'=>'LIE',
'Lithuania'=>'LTU',
'Luxembourg'=>'LUX',
'Macau'=>'MAC',
'Macedonia'=>'MKD',
'Madagascar'=>'MDG',
'Malawi'=>'MWI',
'Malaysia'=>'MYS',
'Maldives'=>'MDV',
'Mali'=>'MLI',
'Malta'=>'MLT',
'Mariana Islands'=>'TIQ',
'MarshallIslands'=>'MHL',
'Martinique'=>'MTQ',
'Mauritania'=>'MRT',
'Mauritius'=>'MUS',
'Mayotte'=>'MYT',
'Mexico'=>'MEX',
'Micronesia'=>'FSM',
'Moldova'=>'MDA',
'Monaco'=>'MCO',
'Mongolia'=>'MNG',
'Montenegro'=>'MNE',
'Montserrat'=>'MSR',
'Morocco'=>'MAR',
'Mozambique'=>'MOZ',
'MyanmarBurma'=>'MMR',
'Namibia'=>'NAM',
'Nauru'=>'NRU',
'NegaraBrunei'=>'BRU',
'Nepal'=>'NPL',
'Netherlands'=>'NLD',
'NetherlandsAntilles'=>'ANT',
'NewCaledonia'=>'NCL',
'NewZealand'=>'NZL',
'Nicaragua'=>'NIC',
'Niger'=>'NER',
'Nigeria'=>'NGA',
'Niue'=>'NIU',
'NorfolkIsland'=>'NFK',
'Norway'=>'NOR',
'Oman'=>'OMN',
'Pakistan'=>'PAK',
'Palau'=>'PLW',
'Palestine'=>'PSE',
'Panama'=>'PAN',
'PapuaNewGuinea'=>'PNG',
'Paraguay'=>'PRY',
'Peru'=>'PER',
'Philippines'=>'PHL',
'Poland'=>'POL',
'Portugal'=>'PRT',
'PuertoRico'=>'PRI',
'Qatar'=>'QAT',
'Reunion'=>'REU',
'Romania'=>'ROU',
'Russia'=>'RUS',
'Rwanda'=>'RWA',
'SaipanNorthernMarianas'=>'MNP',
'SanMarino'=>'SMR',
'SaoTomePrincipe'=>'STP',
'SaudiArabia'=>'SAU',
'Senegal'=>'SEN',
'Senegal Sentel'=>'SNT',
'Serbia'=>'SRB',
'Seychelles'=>'SYC',
'SierraLeone'=>'SLE',
'Singapore'=>'SGP',
'Slovakia'=>'SVK',
'Slovenia'=>'SVN',
'Slovenia IPKO'=>'SVI',
'SolomonIslands'=>'SLB',
'Somalia'=>'SOM',
'South Sudan'=>'SSD',
'SouthAfrica'=>'ZAF',
'Southafrica'=>'ZAF',
'Spain'=>'ESP',
'SriLanka'=>'LKA',
'Sri Lanka'=>'LKA',
'Srilanka'=>'LKA',
'StHelena'=>'SHN',
'StKittsNevis'=>'KNA',
'StLucia'=>'LCA',
'StMartin'=>'SMR',
'StPierreMiquelon'=>'SPM',
'StVincentGrenadines'=>'VCT',
'Sudan'=>'SDN',
'Suriname'=>'SUR',
'Swaziland'=>'SWZ',
'Sweden'=>'SWE',
'Switzerland'=>'CHE',
'Syria'=>'SYR',
'Taiwan'=>'TWN',
'Tajikistan'=>'TJK',
'Tanzania'=>'TZA',
'Thailand'=>'THA',
'Thuraya'=>'TRA',
'Togo'=>'TGO',
'Tokelau'=>'TKL',
'Tonga'=>'TON',
'TrinidadandTobago'=>'TTO',
'Tunisia'=>'TUN',
'Turkey'=>'TUR',
'Turkmenistan'=>'TKM',
'TurksCaicos'=>'TCA',
'Tuvalu'=>'TUV',
'Uganda'=>'UGA',
'Ukraine'=>'UKR',
'UnitedArabEmirates'=>'ARE',
'UnitedStates'=>'USA',
'Uruguay'=>'URY',
'USVirginIslands'=>'VIR',
'Uzbekistan'=>'UZB',
'Vanuatu'=>'VUT',
'Vatican City'=>'VAT',
'Venezuela'=>'VEN',
'Vietnam'=>'VNM',
'WallisFutuna'=>'WLF',
'WesternSamoa'=>'WSM',
'Yemen'=>'YEM',
'Zambia'=>'ZMB',
'Zimbabwe'=>'ZWE',
'Usa'=>'USA',
'USA'=>'USA',
'Uk'=>'GBR',
'United Kingdom'=>'GBR',
'UnitedKingdom'=>'GBR'

);
