(function ($) {
$(document).ready(function(){

$("#freesim_btn_submit").removeAttr("disabled");	

$("#freesim_btn_submit").click(function(event) { 
	event.preventDefault();
	var valid_form = true;
	var userinput = $('#email').val();
	var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i 

	if( !pattern.test(userinput) ) {
		//form-item-email
		$('#FreeSim_divEmail').remove();
		$('#email').addClass('error');
		$('#email').after('<div id="FreeSim_divEmail" class="errormsg">'+ Drupal.t("Please enter an valid Email ID.") + '</div>');
		valid_form = false;
	}
	
	if($('#reg_title').val()==''){
		$('#FreeSim_divTitle').remove();
		$('#reg_title').addClass('error');
		$('#reg_title').after('<div id="FreeSim_divTitle" class="errormsg" >'+ Drupal.t("Please select the Title.") + '</div>');
		valid_form = false;
	}
	
	if($('#first_name').val()==''){
		$('#FreeSim_divFirstName').remove();
		$('#first_name').addClass('error');
		$('#first_name').after('<div id="FreeSim_divFirstName" class="errormsg" >'+ Drupal.t("Please enter First name.") + '</div>');
		valid_form = false;
	}
	if($('#last_name').val()==''){
		//form-item-last-name
		$('#FreeSim_divLastName').remove();
		$('#last_name').addClass('error');
		$('#last_name').after('<div id="FreeSim_divLastName" class="errormsg">'+ Drupal.t("Please enter Last name.") + '</div>');
		valid_form = false;  
	}
	if($('#address1').val()==''){	
		//form-item-address1
		$('#FreeSim_divAddress1').remove();
		$('#address1').addClass('error');
		$('#address1').after('<div id="FreeSim_divAddress1" class="errormsg">'+ Drupal.t("Please enter the Address1.") + '</div>');
		valid_form = false;
	}
	if($('#address2').val()==''){		
		$('#FreeSim_divAddress2').remove();
		$('#address2').after('<div id="FreeSim_divAddress2" class="errormsg">'+ Drupal.t("Please enter the Address2.") + '</div>');
		valid_form = false;
	}
	if($('#town').val()==''){	
		//form-item-town
		$('#FreeSim_divTown').remove();
		$('#town').addClass('error');
		$('#town').after('<div id="FreeSim_divTown" class="errormsg">'+ Drupal.t("Please enter the Town.") + '</div>');
		valid_form = false;
	}
	if($('#pin_code').val()==''){	
		//form-item-pin-code
		$('#FreeSim_divPostCode').remove();
		$('#pin_code').addClass('error');
		$('#pin_code').after('<div id="FreeSim_divPostCode" class="errormsg">'+ Drupal.t("Please enter the Pincode.") + '</div>');
		valid_form = false;
	}

	/* if($('#pre_condition').attr('checked', true)){	
		//form-item-pin-code
		$('#FreeSim_divCondition').remove();
		$('#pre_condition').addClass('error');
		$('#pre_condition').after('<div id="FreeSim_divCondition" class="errormsg">'+ Drupal.t("Please accept the condition.") + '</div>');
		valid_form = false;
	} */		
	if( valid_form == true ){
		$("#freesim_btn_submit").attr("disabled","disabled").html("Loading..");
		$("#freesim-registration-form").submit().once;
	}    
});

$("#freesim-registration-form input").on('keyup' , function() { 
	$(this).next('.errormsg').hide();
	$(this).removeClass('error');
});  

});
})(jQuery);