(function ($) {
$(document).ready(function(){
	/* BEGIN: Step 1 Validation */
	$("#topup_step1").removeAttr("disabled");
	$("#topup_step1").click(function(event) { 
		var valid_form = true;
		var mobile = $("#mobile-number").val();
		var regex = /^[0-9]{10,15}$/;
		if( !regex.test(mobile) ) {
			$('#divTMobile').remove();
			$('#mobile-number').after('<div id="divTMobile" class="errormsg">'+ Drupal.t("Please enter an valid Mobile Number.") + '</div>');
			valid_form = false;
		}
	
		var userinput = $('#email').val();
		var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;

		if( !pattern.test(userinput) ) {
			$('#divTEmail').remove();
			$('#email').after('<div id="divTEmail" class="errormsg">'+ Drupal.t("Please enter an valid email address.") + '</div>');
			valid_form = false;
		}
		if( valid_form == true){
			$("#topup_step1").attr("disabled","disabled").html("Loading..");
			$("#topup-registration-form").submit();
		}else{
			event.preventDefault();
		}  
	});
	$("#topup-registration-form input").on('keyup' , function() { 
		$(this).next('.errormsg').hide();
	});
	/* END: Step 1 Validation */
	
	/* BEGIN: CC Step 2 Validation */
	$("#topup_step2").removeAttr("disabled");
	$("#topup_step2").click(function(event) { 
		var valid_form = true;
		if( $.trim($('#cardholder_name').val()) == ''){
			$('#divTCName').remove();
			$('#cardholder_name').after('<div id="divTCName" class="errormsg">'+ Drupal.t("Please enter the Card Holder Name.") + '</div>');
			valid_form = false;
		}
		if( $.trim($('#card_number').val()) == ''){
			$('#divTCNumber').remove();
			$('#card_number').after('<div id="divTCNumber" class="errormsg">'+ Drupal.t("Please enter the Card Number.") + '</div>');
			valid_form = false;
		}
		if( $.trim($('#edit-expiry-title-month').val()) == '' || $.trim($('#edit-expiry-title-year').val()) == '' ){
			$('#divTCEDate').remove();
			$('#edit-expiry-title').after('<div id="divTCEDate" class="errormsg">'+ Drupal.t("Please select the card expiry date & month.") + '</div>');
			valid_form = false;
		}
		if( $.trim($('#security_code').val()) == ''){
			$('#divTCVV').remove();
			$('#security_code').after('<div id="divTCVV" class="errormsg">'+ Drupal.t("Please enter the Card Security Code.") + '</div>');
			valid_form = false;
		}
		if( $.trim($('#address1').val()) == ''){
			$('#divTaddress1').remove();
			$('#address1').after('<div id="divTaddress1" class="errormsg">'+ Drupal.t("Please enter the Address1.") + '</div>');
			valid_form = false;
		}
		if( $.trim($('#address2').val()) == ''){
			$('#divTaddress2').remove();
			$('#address2').after('<div id="divTaddress2" class="errormsg">'+ Drupal.t("Please enter the Address2.") + '</div>');
			valid_form = false;
		}
		if( $.trim($('#town').val()) == ''){
			$('#divTtown').remove();
			$('#town').after('<div id="divTtown" class="errormsg">'+ Drupal.t("Please enter the Town Name.") + '</div>');
			valid_form = false;
		}
		if( $.trim($('#pin_code').val()) == ''){
			$('#divTpin_code').remove();
			$('#pin_code').after('<div id="divTpin_code" class="errormsg">'+ Drupal.t("Please enter the Postal Code.") + '</div>');
			valid_form = false;
		}
		if( !$("#condition").is(":checked") ){
			$('#divTcondition').remove();
			$('.form-item-condition').after('<div id="divTcondition" class="errormsg">'+ Drupal.t("Please accept the terms & conditions.") + '</div>');
			valid_form = false;
		}		
		
		if( valid_form == true){
			$("#topup_step2").attr("disabled","disabled").html("Loading..");
			$("#topup-payment-form").submit();
		}else{
			event.preventDefault();
		}  
	});
	$("#topup-payment-form input").on('keyup' , function() {
		$(this).next('.errormsg').hide();
	});
	$("#topup-payment-form select").change(function() {
		$(this).parent().parent().parent().next('.errormsg').hide();
	});	
	/* END: CC Step 2 Validation */
	
	/* BEGIN: Paysafe Step 2 Validation */
	$("#paysafe_step2").removeAttr("disabled");
	$("#paysafe_step2").click(function(event) { 
		var valid_form = true;
		if( $.trim($('#name').val()) == ''){
			$('#divTCName').remove();
			$('#name').after('<div id="divTCName" class="errormsg">'+ Drupal.t("Please enter the Name.") + '</div>');
			valid_form = false;
		}
		if( $.trim($('#address1').val()) == ''){
			$('#divTaddress1').remove();
			$('#address1').after('<div id="divTaddress1" class="errormsg">'+ Drupal.t("Please enter the House Number.") + '</div>');
			valid_form = false;
		}
		if( $.trim($('#address2').val()) == ''){
			$('#divTaddress2').remove();
			$('#address2').after('<div id="divTaddress2" class="errormsg">'+ Drupal.t("Please enter the Street Name.") + '</div>');
			valid_form = false;
		}
		if( $.trim($('#town').val()) == ''){
			$('#divTtown').remove();
			$('#town').after('<div id="divTtown" class="errormsg">'+ Drupal.t("Please enter the Town Name.") + '</div>');
			valid_form = false;
		}
		if( $.trim($('#pin_code').val()) == ''){
			$('#divTpin_code').remove();
			$('#pin_code').after('<div id="divTpin_code" class="errormsg">'+ Drupal.t("Please enter the Postal Code.") + '</div>');
			valid_form = false;
		}
		if( !$("#condition").is(":checked") ){
			$('#divTcondition').remove();
			$('.form-item-condition').after('<div id="divTcondition" class="errormsg">'+ Drupal.t("Please accept the terms & conditions.") + '</div>');
			valid_form = false;
		}		
		
		if( valid_form == true){
			$("#paysafe_step2").attr("disabled","disabled").html("Loading..");
			$("#topup-paysafe-step2-form").submit();
		}else{
			event.preventDefault();
		}  
	});
	$("#topup-paysafe-step2-form input").on('keyup' , function() { 
		$(this).next('.errormsg').hide();
	});
	/* END: Paysafe Step 2 Validation */	

	//Validate Mobile Number STEP1
	$("#topup-registration-form #mobile-number").on('blur' , function(){ 
		if($.trim($(this).val() != '')){
			$('#divTMobile').remove();
			$.ajax({
				url: Drupal.settings.basePath + 'topup/mobile_validation',
				type: "POST",
				data:{
					mobile_number: $(this).val()
				},
				success: function(data){	
					if(data == -4){ //Mobile number is InActive					
						$('#mobile-number').after('<div id="divTMobile" class="errormsg">'+ Drupal.t('Your Sim Is NOT ACTIVE. Please Contact Customer Service.') + '</div>');
					}else if(data == -3){ //Mobile brand is not valid
						$('#mobile-number').after('<div id="divTMobile" class="errormsg">'+ Drupal.t('Please try with Vectone Mobile Number.') + '</div>');
					}else if(data == -2){ //PAYM number is not allowing for Topup.
						$('#mobile-number').after('<div id="divTMobile" class="errormsg">'+ Drupal.t('The PAYM Number is not allowed for Topup Process.') + '</div>');
					}else if(data == -1){ //Mobile number not found.
						$('#mobile-number').after('<div id="divTMobile" class="errormsg">'+ Drupal.t('Please enter the valid mobile number and try again.') + '</div>');
					}else{
						$('#divTMobile').remove();
					}
				}
			});
		}
	});
	
});
})(jQuery);